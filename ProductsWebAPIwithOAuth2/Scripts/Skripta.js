﻿$(document).ready(function () {

    // podaci od interesa
    var host = 'http://' + window.location.host;
    var token = null;
    var headers = {};
    var formAction = "Create";
    var editingId = "";

    // posto inicijalno nismo prijavljeni, sakrivamo odjavu
    $("#odjava").css("display", "none");

    // Punjenje tabele podacima
    populateTable();

    // pripremanje dogadjaja za na klik
    $("body").on("click", "#btnAdd", addItem);
    $("body").on("click", "#btnDeleteItem", deleteItem);
    $("body").on("click", "#btnEditItem", editItem);

    // registracija korisnika
    $("#registracija").submit(function (e) {
        e.preventDefault();

        var email = $("#regEmail").val();
        var loz1 = $("#regLoz").val();
        var loz2 = $("#regLoz2").val();

        // objekat koji se salje
        var sendData = {
            "Email": email,
            "Password": loz1,
            "ConfirmPassword": loz2
        };

        $.ajax({
            type: "POST",
            url: host + "/api/Account/Register",
            data: sendData

        }).done(function (data) {
            $("#info").append("Uspešna registracija. Možete se prijaviti na sistem.");

        }).fail(function (data) {
            alert(data);
        });
    });


    // prijava korisnika
    $("#prijava").submit(function (e) {
        e.preventDefault();

        var email = $("#priEmail").val();
        var loz = $("#priLoz").val();

        // objekat koji se salje
        var sendData = {
            "grant_type": "password",
            "username": email,
            "password": loz
        };

        $.ajax({
            "type": "POST",
            "url": host + "/Token",
            "data": sendData

        }).done(function (data) {
            console.log(data/*.userName + "\n" + data.expires_in + "\n" + data.token_type + "\n" + data.access_token*/);
            $("#info").empty().append("Prijavljen korisnik: " + data.userName);
            token = data.access_token;
            $("#prijava").css("display", "none");
            $("#registracija").css("display", "none");
            $("#odjava").css("display", "block");

            let showElements = document.getElementsByClassName("hideItem");
            let divAddEdit = document.getElementById("divAddEdit");
            while (showElements.length) {
                showElements[0].classList.add("showItem");
                showElements[0].classList.remove("hideItem");
            }
            divAddEdit.classList.add("hideItem");

        }).fail(function (data) {
            alert(data);
        });
    });

    // odjava korisnika sa sistema
    $("#odjaviSe").click(function () {
        token = null;
        headers = {};

        $("#prijava").css("display", "block");
        $("#registracija").css("display", "block");
        $("#odjava").css("display", "none");
        $("#info").empty();
        $("#sadrzaj").empty();

        let showElements = document.getElementsByClassName("showItem");
        while (showElements.length) {
            showElements[0].classList.remove("showItem");
            showElements[0].classList.add("hideItem");
        }
    })

    // ucitavanje prvog proizvoda
    $("#proizvodi").click(function () {
        // korisnik mora biti ulogovan
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        $.ajax({
            "type": "GET",
            "url": host + "/api/products/1",
            "headers": headers

        }).done(function (data) {
            $("#sadrzaj").append("Proizvod: " + data.Name);

        }).fail(function (data) {
            alert(data.status + ": " + data.statusText);
        });
    });

    function populateTable() {
        $.ajax({
            type: "GET",
            url: host + "/api/products"
        })
            .done(function (data, status) {
                $("#tableData1").empty();
                //$("#info").append("Uspešna registracija. Možete se prijaviti na sistem.");
                var table1 = document.getElementById('table1');
                var body1 = document.getElementById('table1').getElementsByTagName('tbody')[0];
                for (var i = 0; i < data.length; i++) {
                    // Insert a row in the table at the last row
                    //var row1 = table1.insertRow(table1.rows.length);
                    var row1 = body1.insertRow(i);
                    row1.classList.add("text-center");

                    // Insert a cell in the row at index
                    var cell0 = row1.insertCell(0);
                    var cell1 = row1.insertCell(1);
                    var cell2 = row1.insertCell(2);
                    var cell3 = row1.insertCell(3);
                    var cell4 = row1.insertCell(4);

                    // Add some text to the new cells:
                    cell0.innerHTML = data[i].Name;
                    cell1.innerHTML = data[i].Price;
                    cell2.innerHTML = data[i].Date;
                    if (!token) {
                        cell3.classList.add("hideItem");
                        cell3.classList.remove("showItem");
                        cell4.classList.add("hideItem");
                        cell4.classList.remove("showItem");
                    }
                    //cell3.style.display = "none";
                    //cell4.style.display = "none";

                    //var btn1 = document.createElement('input');
                    //var btn2 = document.createElement('input');
                    var btn1 = document.createElement('button');
                    var btn2 = document.createElement('button');
                    //btn1.type = "button";
                    //btn2.type = "button";
                    btn1.className = "btn";
                    btn2.className = "btn";
                    btn1.textContent = "Edit";
                    btn2.textContent = "Delete";
                    btn1.value = data[i].Id;
                    btn2.value = data[i].Id;
                    btn1.name = "id";
                    btn2.name = "id";
                    btn1.id = "btnEditItem";
                    btn2.id = "btnDeleteItem";
                    cell3.appendChild(btn1);
                    cell4.appendChild(btn2);

                    // Append a text node to the cell
                    //var newText = document.createTextNode('New row');
                    //newCell1.appendChild(newText);
                    //table1.classList.add('text-center');
                }
            })
            .fail(function (data, status) {
                alert(data);
            });
    }

    // brisanje stavke
    function deleteItem() {
        // izvlacimo {id}
        var deleteID = this.value;
        // saljemo zahtev 
        $.ajax({
            url: host + "/api/products/" + deleteID.toString(),
            type: "DELETE",
        })
            .done(function (data, status) {
                populateTable();
            })
            .fail(function (data, status) {
                alert("Desila se greska prilikom brisanja stavke!");
            });
    };

    function addItem() {
        var editId = this.value;
        if (token) {
            let divTable1 = document.getElementById("divTable1");
            divTable1.classList.remove("showItem");
            divTable1.classList.add("hideItem");

            let divAddEdit = document.getElementById("divAddEdit");
            divAddEdit.classList.add("showItem");
            divAddEdit.classList.remove("hideItem");

            $("#addEditForm").submit(function (e) {
                e.preventDefault();

                var itemName = $("#itemName").val();
                var itemPrice = $("#itemPrice").val();
                var itemDate = $("#itemDate").val();
                var url = "";
                var httpMethod = "";
                var sendData = {};

                // objekat koji se salje
                sendData = {
                    "Name": itemName,
                    "Price": itemPrice,
                    "Date": itemDate
                };

                if (formAction === "Create") {
                    url = host + "/api/products";
                    httpMethod = "POST";
                    sendData = {
                        "Name": itemName,
                        "Price": itemPrice,
                        "Date": itemDate
                    };
                } else {
                    httpMethod = "PUT";
                    url = host + "/api/products" + editId.toString();
                    sendData = {
                        "Id": editId,
                        "Name": itemName,
                        "Price": itemPrice,
                        "Date": itemDate
                    };
                }

                $.ajax({
                    url: url,
                    type: httpMethod,
                    data: sendData
                })
                    .done(function (data, status) {
                        populateTable();
                        let divTable1 = document.getElementById("divTable1");
                        let divAddEdit = document.getElementById("divAddEdit");
                            divAddEdit.classList.add("hideItem");
                            divAddEdit.classList.remove("showItem");
                            divTable1.classList.remove("hideItem");
                            divTable1.classList.add("showItem");
                    })
                    .fail(function (data, status) {
                        if (formAction === "Create") {
                            alert("Desila se greska prilikom dodavanja nove stavke!\n" + data);
                        } else {
                            alert("Desila se greska prilikom editovanja stavke!\n" + data);
                        }
                    })
            });

        } else {
            alert("You must be loged in to add item.")
        }
    }

    function editItem() {
        var editId = this.value;
        if (token) {
            headers.Authorization = 'Bearer ' + token;
            let divTable1 = document.getElementById("divTable1");
            divTable1.classList.remove("showItem");
            divTable1.classList.add("hideItem");

            let divAddEdit = document.getElementById("divAddEdit");
            divAddEdit.classList.add("showItem");
            divAddEdit.classList.remove("hideItem");

            // saljemo zahtev da dobavimo stavku
            $.ajax({
                url: host + "/api/products/" + editId.toString(),
                type: "GET",
                "headers": headers
            })
                .done(function (data, status) {
                    $("#itemName").val(data.Name);
                    $("#itemPrice").val(data.Price);
                    $("#itemDate").val(data.Date);
                    editingId = data.Id;
                    formAction = "Update";
                })
                .fail(function (data, status) {
                    formAction = "Create";
                    alert("Desila se greska!");
                });
        } else {
            alert("You must be loged in to add item.")
        }
    }
});