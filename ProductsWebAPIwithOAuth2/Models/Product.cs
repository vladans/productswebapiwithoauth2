﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProductsWebAPIwithOAuth2.Models
{
    // (Id, Naziv, Cena, Datum proizvodnje)
    public class Product
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public decimal Price { get; set; }
        public DateTime Date { get; set; }

    }
}