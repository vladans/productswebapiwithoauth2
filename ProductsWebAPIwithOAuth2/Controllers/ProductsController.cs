﻿using ProductsWebAPIwithOAuth2.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProductsWebAPIwithOAuth2.Controllers
{
    // !!! NAPOMENA za filter [Authorize] je potrebano ukljuciti --> using System.Web.Http;
    //[Authorize]
    public class ProductsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        //[AllowAnonymous]
        public IEnumerable<Product> GetAll()
        {
            if (User.IsInRole("Administrators"))
            {
                // ...
            }
            return db.Products.OrderBy(x => x.Name);
        }

        //// Restrict by user
        //[Authorize(Users="Alice,Bob")]
        //// Restrict by role
        //[Authorize(Roles="Administrators")]
        [Authorize]
        public IHttpActionResult GetById(int id)
        {
            var product = db.Products.FirstOrDefault(x => x.Id == id);
            if (product == null)
            {
                return NotFound();
            }
            return Ok(product);
        }

        public IHttpActionResult Post(Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Products.Add(product);
            int res = db.SaveChanges();
            if (res <= 0)
            {
                return BadRequest();
            }

            return CreatedAtRoute("DefaultApi", new { id = product.Id }, product);
        }

        public IHttpActionResult Put(int id, Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != product.Id)
            {
                return BadRequest();
            }

            try
            {
                db.Entry(product).State = EntityState.Modified;
                try
                {
                    int res = db.SaveChanges();
                    if (res <= 0)
                    {
                        return BadRequest();
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
            }
            catch
            {
                return BadRequest();
            }

            return Ok(product);
        }

        public IHttpActionResult Delete(int id)
        {
            var product = db.Products.FirstOrDefault(x => x.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            db.Products.Remove(product);
            int res = db.SaveChanges();
            if (res <= 0)
            {
                return BadRequest();
            }
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        //public void Dispose()
        //{
        //    Dispose(true);
        //    GC.SuppressFinalize(this);
        //}
    }
}
