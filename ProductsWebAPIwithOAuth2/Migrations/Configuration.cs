namespace ProductsWebAPIwithOAuth2.Migrations
{
    using ProductsWebAPIwithOAuth2.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ProductsWebAPIwithOAuth2.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ProductsWebAPIwithOAuth2.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Products.AddOrUpdate(x => x.Id,
                new Product() { Id = 1, Name = "Chees", Price = 10m, Date = DateTime.Now }, // Date = Convert.ToDateTime("24/11/2018")
                new Product() { Id = 2, Name = "Milk", Price = 20m, Date = DateTime.Now },
                new Product() { Id = 3, Name = "Bread", Price = 30m, Date = DateTime.Now },
                new Product() { Id = 4, Name = "Wather", Price = 40m, Date = DateTime.Now },
                new Product() { Id = 5, Name = "Coffe", Price = 50m, Date = DateTime.Now },
                new Product() { Id = 6, Name = "Sugar", Price = 60m, Date = DateTime.Now }
            );
        }
    }
}
